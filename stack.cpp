#include <iostream>
#include "stack.h"

void initStack(stack* s)
{
	s->_count = 0;
}

void cleanStack(stack* s)
{
	for (int i = 0; i < s->_count; i++)
	{
		linkedList* temp = s->head;
		s->head = s->head->_next;
		delete temp;
	}
}

void push(stack* s, int element)
{
	if (element >= 0)
	{
		addVal(element, &(s->head));
		s->_count++;
	}
}

int pop(stack* s)
{
	int ret = -1;
	if (s->_count > 0)
	{
		ret = remVal(&(s->head));
		s->_count--;
	}
	return ret;
}