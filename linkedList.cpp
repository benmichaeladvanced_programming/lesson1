#include "linkedList.h"

void addVal(int val, linkedList** head)
{
	linkedList* node = new linkedList;
	node->_val = val;
	node->_next = *head;
	*head = node;
}

int remVal(linkedList** head)
{
	linkedList* tmp = *head;
	int ret = tmp->_val;
	*head = tmp->_next;
	delete tmp;
	return ret;
}