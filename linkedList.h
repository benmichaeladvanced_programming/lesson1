struct linkedList
{
	linkedList* _next;
	int _val;
};
typedef struct linkedList linkedList;

void addVal(int val, linkedList** head);
int remVal(linkedList** head);