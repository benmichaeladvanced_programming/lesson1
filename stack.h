#ifndef _STACK_H
#define _STACK_H

#include "linkedList.h"

/* a positive-integer value stack, with no size limit */
typedef struct
{
	linkedList* head;
	int _count;
} stack;

void push(stack *s, int element);
int pop(stack *s);

void initStack(stack *s);
void cleanStack(stack *s);


#endif /* _STACK_H */